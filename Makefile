# Render markdown into an I-D
#
# Prerequisites: ruby-kramdown-rfc2629 xml2rfc GNU-Make python
#
# For pdf output, also: weasyprint
# Tested with the Debian versions of the tools

DRAFT_DIR := $(strip $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST)))))

###########################################################################
# Override these defaults in ./Makefile.settings, or in the environment   #
###########################################################################

include ./Makefile.settings

DRAFT_BASENAME ?= $(shell basename $(DRAFT_DIR))
DRAFT_FULLNAME ?= $(DRAFT_BASENAME)
DOCKER_CMD ?= docker

###########################################################################

OUTPUT := $(DRAFT_BASENAME).txt $(DRAFT_BASENAME).html $(DRAFT_BASENAME).xml
all: $(OUTPUT)

$(DRAFT_BASENAME).md: $(wildcard draft-gitlab-template.md)
	git mv draft-gitlab-template.md $(DRAFT_BASENAME).md
	sed -i.bak -e "s/^docname: .*$$/docname: $(DRAFT_FULLNAME)-00/" $(DRAFT_BASENAME).md
	rm $(DRAFT_BASENAME).md.bak

init: $(DRAFT_BASENAME).md
	git commit -am "Renaming"
	bash -c 'git reset $$(git commit-tree "HEAD^{tree}" -m "Initial commit")'

NEWVERSION = $(shell awk -F- '/^docname: / { printf "%02i\n", ($$NF + 1) ; exit }' $(DRAFT_BASENAME).md)
bumpver:
	sed -i.bak -e "s/^docname: .*$$/docname: $(DRAFT_FULLNAME)-$(NEWVERSION)/" $(DRAFT_BASENAME).md
	rm $(DRAFT_BASENAME).md.bak

latestver:
	sed -i.bak -e "s/^\(docname: .*\)-[0-9][0-9]$$/\1-latest/" ${DRAFT_BASENAME}.md
	rm $(DRAFT_BASENAME).md.bak

%.xml: %.md $(wildcard test-vectors/*)
	kramdown-rfc2629 --v3 $< > $@.tmp
	mv $@.tmp $@

publishxml: $(DRAFT_BASENAME).xml $(DRAFT_BASENAME).pdf $(DRAFT_BASENAME).txt
	mkdir -p public
	zip -r public/$(DRAFT_BASENAME).zip $(DRAFT_BASENAME).xml $(DRAFT_BASENAME).pdf $(DRAFT_BASENAME).txt

%.html: %.xml
	xml2rfc --v3 $< --html

publishhtml: $(DRAFT_BASENAME).html
	mkdir -p public
	cp $(DRAFT_BASENAME).html public/index.html

%.txt: %.xml
	xml2rfc --v3 $< --text

%.pdf: %.xml
	xml2rfc --v3 $< --pdf

# This currently doesn't work
$(DRAFT_BASENAME).txt.diff: $(DRAFT_BASENAME).txt compare canonicalizetxt
	! ./compare > $@.tmp
	mv $@.tmp $@

# builds a docker image that can build the targets in this makefile
# see kramdown-rfc2629-docker/Dockerfile for documentation, it's only three lines
docker-image:
	$(DOCKER_CMD) build -t kramdown-rfc2629-docker kramdown-rfc2629-docker/

# runs "make all" in a docker container that mounts this directory as a volume
# this takes the UID and GID from the current env, so the output files don't belong the root
docker-all:
	$(DOCKER_CMD) run --rm -i --user ${UID}:${GID} -e DRAFT_BASENAME=$(DRAFT_BASENAME) -v $(PWD):/rfc kramdown-rfc2629-docker:latest make

# drops you in a shell in the container, with this directory mounted where you can run "make" more quickly
# this takes the UID and GID from the current env, so the output files don't belong the root
docker-shell:
	$(DOCKER_CMD) run --rm -it --user ${UID}:${GID} -e DRAFT_BASENAME=$(DRAFT_BASENAME) -v $(PWD):/rfc kramdown-rfc2629-docker:latest bash

clean:
	-rm -rf $(OUTPUT) *.tmp $(DRAFT_BASENAME).txt.diff $(DRAFT_BASENAME).md.reflowed

check: codespell check-reflow trailing-whitespace

check-reflow:
	./reflow < $(DRAFT_BASENAME).md > $(DRAFT_BASENAME).md.reflowed
	diff -u $(DRAFT_BASENAME).md $(DRAFT_BASENAME).md.reflowed

codespell: .codespell-ignore
	codespell --ignore-words .codespell-ignore $(DRAFT_BASENAME).md

trailing-whitespace:
	! grep -n '[[:space:]]$$' $(DRAFT_BASENAME).md

.PHONY: clean all check codespell check-reflow
