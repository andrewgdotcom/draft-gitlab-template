# Internet Draft Template

This repository holds a template and toolkit for authoring RFCs on GitLab.
It is based on the tooling used by the OpenPGP WG.

## How to use

* Clone this repository and rename the directory.
* Set `DRAFT_BASENAME` in `Makefile.settings` to set the name of the source file to be created.
    This will typically be the name of your gitlab repo, e.g. `WORKINGGROUP-SUBJECT`.
* Set `DRAFT_FULLNAME` in `Makefile.settings` to set the name of the published draft, if different from `$DRAFT_BASENAME`.
    This will typically be `draft-YOURNAME-WORKINGGROUP-SUBJECT` or `draft-ietf-WORKINGGROUP-SUBJECT`.
* Incant `make init` to create the `$DRAFT_BASENAME.md` file and reset the git history.
* Incant `git remote set-url origin git@gitlab.com:YOURACCOUNTNAME/$DRAFT_BASENAME` to reset the origin.
* Edit the `$DRAFT_BASENAME.md` file to taste.
* Commit your changes, and push.

Your formatted draft will now be visible at `https://YOURACCOUNTNAME.gitlab.io/$DRAFT_BASENAME` .

The other generated files will be available for download at `https://YOURACCOUNTNAME.gitlab.io/$DRAFT_BASENAME/$DRAFT_BASENAME.zip` .
The `.xml` file in this archive can be uploaded to https://datatracker.ietf.org/submit/ .

### Bumping the version number

To bump the version number of your draft (recommended to do so immediately after publishing the previous version!), incant:

    make bumpver

## To build on your local machine

Install the project dependencies (see the Makefile for details) and run `make all` to build the output files in this directory.

Alternatively, you can build a docker image with the dependencies pre-installed:

* Install docker
* Run `make docker-image`

Now you can run `make docker-all` to build the output files.

The `.xml` file can then be uploaded to https://datatracker.ietf.org/submit/ .

## Local settings override

You can specify the following in `Makefile.settings`, or in your environment:

* `DRAFT_BASENAME` overrides the name of the files containing the draft (defaults to the repo directory name).
* `DRAFT_FULLNAME` overrides the name of the published internet draft (defaults to `DRAFT_BASENAME`).
* `DOCKER_CMD` overrides the default container engine (EXPERIMENTAL).
