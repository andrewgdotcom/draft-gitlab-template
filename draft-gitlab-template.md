---
title: "((TO BE COMPLETED))"
abbrev: "((TO BE COMPLETED))"
category: info
docname: draft-gitlab-template-00
ipr: trust200902
area: int
workgroup: ((TO BE COMPLETED))
keyword: Internet-Draft
submissionType: IETF
venue:
  group: "((WORKING GROUP))"
  type: "Working Group"
  mail: "((EMAIL))"
  arch: "((MAILING LIST ARCHIVE))"
  repo: "((REPO URL))"
  latest: "((GITLAB PAGE URL))"
stand_alone: yes
pi: [toc, sortrefs, symrefs]
author:
 -
    fullname: Andrew Gallagher
    organization: PGPKeys.EU
    email: andrewg@andrewg.com
    role: editor
normative:
  RFC4880:
informative:
  SHAMBLES:
    target: https://sha-mbles.github.io/
    title: "Sha-1 is a shambles: First chosen-prefix collision on sha-1 and application to the PGP web of trust"
    author:
      -
        name: Gaëtan Leurent
      -
        name: Thomas Peyrin
    date: 2020

--- abstract

This document specifies ((TO BE COMPLETED))

--- middle

# Introduction {#introduction}

((TO BE COMPLETED))

# Conventions and Definitions {#conventions-definitions}

{::boilerplate bcp14-tagged}

# Section Name {#section-name}

This document uses Kramdown-rfc2629 format.
Put one sentence per line, this makes `git diff` work better.
You can refer to an anchor link (such as a section or table) using {{section-name}}.
You can refer to a normative or informative document using [RFC4880], [SHAMBLES].

{: title="Here is how you insert a table" #how-to-insert-a-table}
Field     | Description
----------|-------------------------
version   | the version of this output format
count     | the number of items

# Security Considerations {#security-considerations}

((TO BE COMPLETED))

# IANA Considerations {#iana-considerations}

This document assigns the following identifiers: ((TO BE COMPLETED))

--- back

# Acknowledgments

The authors would like to thank ((TO BE COMPLETED)).
